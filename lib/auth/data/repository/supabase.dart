import 'package:rch_2024/auth/data/models/model_auth.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

var supabase = Supabase.instance.client;

Future<AuthResponse> signIn(
  ModelAuth modelAuth,
) async {
  return await supabase.auth.signInWithPassword(
    email: modelAuth.email,
    password: modelAuth.password
  );
}

Future<AuthResponse> signUp(
  ModelAuth modelAuth,
) async {
  return await supabase.auth.signUp(
      email: modelAuth.email,
      password: modelAuth.password
  );
}

Future<void> logout() async {
  await supabase.auth.signOut();
}

Future<void> sendCode(String email) async {
  await supabase.auth.resetPasswordForEmail(email);
}

Future<AuthResponse> verifyOtpCode(String email, String code) async {
  return await supabase.auth.verifyOTP(
    token: code,
    type: OtpType.email,
    email: email
  );
}

Future<UserResponse> updatePassword(String newPassword) async {
  return await supabase.auth.updateUser(UserAttributes(password: newPassword));
}