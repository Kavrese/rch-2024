import 'dart:async';

import 'package:rch_2024/auth/data/repository/supabase.dart';
import 'package:rch_2024/common/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class OtpPresenter {

  var lostSecond = 60;

  void decrementSecond(){
    if (lostSecond != 0){
      lostSecond --;
    }
  }

  void startDelayDecrement(Function(int) callback){
    Timer(const Duration(seconds: 1), () {
      decrementSecond();
      callback(lostSecond);
      startDelayDecrement(callback);
    });
  }

  Future<void> resendCode(
    String email,
    Function() onResponse,
    Function(String) onError
  ) async {
    request(
      request: () async {
        await sendCode(email);
      },
      onResponse: (_){
        lostSecond = 60;
        onResponse();
      },
      onError: onError
    );
  }

  Future<void> verifyCode(
    String email,
    String code,
    Function(AuthResponse) onResponse,
    Function(String) onError
  ) async {
    request(
      request: () async {
        return await verifyOtpCode(email, code);
      },
      onResponse: onResponse,
      onError: onError
    );
  }
}