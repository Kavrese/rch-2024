import 'package:rch_2024/auth/data/repository/supabase.dart';
import 'package:rch_2024/common/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class SetNewPasswordPresenter {

  bool isValid(String password, String confirmPassword){
    return password == confirmPassword;
  }

  Future<void> pressConfirmNewPassword(
    String newPassword,
    Function(UserResponse) onResponse,
    Function(String) onError
  ) async {
    request(
      request: () async {
        var res = await updatePassword(newPassword);
        await logout();
        return res;
      },
      onResponse: onResponse,
      onError: onError
    );
  }
}