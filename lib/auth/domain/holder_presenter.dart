import 'package:rch_2024/auth/data/repository/supabase.dart';
import 'package:rch_2024/common/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HolderPresenter {

  late SharedPreferences sharedPreferences;

  HolderPresenter(){
    SharedPreferences.getInstance().then((value) => sharedPreferences = value);
  }

  Future<void> pressExitButton(
    Function() onResponse,
    Function(String) onError
  ) async {
    await request(
      request: logout,
      onResponse: (_) async {
        await sharedPreferences.clear();
        onResponse();
      },
      onError: onError
    );
  }
}