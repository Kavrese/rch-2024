import 'package:rch_2024/auth/data/repository/supabase.dart';
import 'package:rch_2024/common/utils.dart';

class ForgotPasswordPresenter {
  bool isValid(String email){
    return checkEmail(email);
  }

  Future<void> pressSendCode(
    String email,
    Function(void) onResponse,
    Function(String) onError
  ) async {
    request(
      request: () async {
        await sendCode(email);
      },
      onResponse: onResponse,
      onError: onError
    );
  }
}