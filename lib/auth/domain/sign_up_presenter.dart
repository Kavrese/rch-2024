import 'package:flutter/material.dart';
import 'package:rch_2024/auth/data/models/model_auth.dart';
import 'package:rch_2024/auth/data/repository/supabase.dart';
import 'package:rch_2024/common/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class SignUpPresenter {

  final BuildContext context;

  SignUpPresenter({required this.context});

  bool isValid(
    String email,
    String password,
    String confirmPassword
  ){
    return email.isNotEmpty && password.isNotEmpty &&
        confirmPassword == password && checkEmail(email);
  }

  void pressSignUp(
    String email,
    String password,
    Function(AuthResponse) onResponse
  ){
    ModelAuth modelAuth = ModelAuth(email: email, password: password);
    request(
      request: () async { return await signUp(modelAuth); },
      onResponse: onResponse,
      onError: (error){
        showErrorDialog(context, error);
      }
    );
  }
}