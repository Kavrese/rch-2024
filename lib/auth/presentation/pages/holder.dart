import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rch_2024/auth/domain/holder_presenter.dart';
import 'package:rch_2024/common/app.dart';
import 'package:rch_2024/common/utils.dart';

class Holder extends StatefulWidget {
  const Holder({super.key});

  @override
  State<Holder> createState() => _HolderState();
}

class _HolderState extends State<Holder> {
  
  var presenter = HolderPresenter();
  
  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 22),
          child: SizedBox(
            width: double.infinity,
            child: FilledButton(
              onPressed: () {
                presenter.pressExitButton(
                  () {
                    exit(0);
                  },
                  (error) {
                    showErrorDialog(context, error);
                  }
                );
              },
              child: const Text("ВЫХОД"),
            ),
          ),
        ),
      ),
    );
  }
}