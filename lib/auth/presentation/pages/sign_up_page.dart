import 'package:flutter/material.dart';
import 'package:rch_2024/auth/domain/sign_up_presenter.dart';
import 'package:rch_2024/auth/presentation/pages/holder.dart';
import 'package:rch_2024/auth/presentation/pages/sign_in_page.dart';
import 'package:rch_2024/common/app.dart';
import 'package:rch_2024/common/controllers/password_text_controller.dart';
import 'package:rch_2024/common/utils.dart';
import 'package:rch_2024/common/widgets/custom_text_field.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  bool isValid = false;

  late SignUpPresenter presenter;


  @override
  void initState() {
    super.initState();
    presenter = SignUpPresenter(context: context);
  }

  var email = TextEditingController();
  var password = PasswordTextController();
  var confirmPassword = TextEditingController();

  var obscurePassword = true;
  var obscureConfirmPassword = true;

  var policy = false;
  var enableButton = false;

  void onChange(_){
    setState(() {
      isValid = presenter.isValid(
        email.text,
        password.text,
        confirmPassword.text
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      backgroundColor: colors.background,
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 78),
            Text(
              "Создать аккаунт",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w700,
                color: colors.text
              )
            ),
            const SizedBox(height: 8),
            Text(
              "Завершите регистрацию чтобы начать",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w700,
                color: colors.subText
              )
            ),
            const SizedBox(height: 28),
            CustomTextField(
                label: "Почта",
                hint: "***********@mail.com",
                onChange: onChange,
                controller: email),
            const SizedBox(height: 24),
            CustomTextField(
              label: "Пароль",
              hint: "***********",
              controller: password,
              enableObscure: true,
              onChange: onChange
            ),
            const SizedBox(height: 24),
            CustomTextField(
              label: "Повторите пароль",
              hint: "***********",
              controller: confirmPassword,
              enableObscure: true,
              onChange: onChange
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                      onPressed: (isValid) ? (){
                        showLoading(context);
                        presenter.pressSignUp(
                          email.text,
                          password.getHashText(),
                          (_) {
                            Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(builder: (_) => const Holder()),
                              (route) => false
                            );
                          }
                        );
                        Navigator.pop(context);
                      } : null,
                      child: Text("Зарегистрироваться", style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: colors.textAccent
                      ))
                    ),
                  ),
                  const SizedBox(height: 14),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (_) => const SignInPage())
                      );
                    },
                    child: RichText(text: TextSpan(
                      children: [
                        TextSpan(
                          text: "У меня уже есть аккаунт! ",
                          style: TextStyle(fontSize: 14, color: colors.subText)
                        ),
                        TextSpan(
                          text: "Войти",
                          style: TextStyle(
                            fontSize: 14,
                            color: colors.accent,
                            fontWeight: FontWeight.w700
                          )
                        ),
                      ]
                    )),
                  ),
                  const SizedBox(height: 34),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}