import 'package:flutter/material.dart';
import 'package:rch_2024/auth/domain/forgot_password_presenter.dart';
import 'package:rch_2024/auth/presentation/pages/page_otp.dart';
import 'package:rch_2024/auth/presentation/pages/sign_in_page.dart';
import 'package:rch_2024/common/app.dart';
import 'package:rch_2024/common/utils.dart';
import 'package:rch_2024/common/widgets/custom_text_field.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({super.key});

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  ForgotPasswordPresenter presenter = ForgotPasswordPresenter();

  var enableButton = false;
  var email = TextEditingController();

  void onChange(_) {
    setState(() {
      enableButton = presenter.isValid(email.text);
    });
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 83),
            Text(
              "Восстановление пароля",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w700,
                color: colors.text
              ),
            ),
            const SizedBox(height: 8),
            Text(
              "Введите свою почту",
              style: TextStyle(
                fontSize: 14,
                color: colors.subText,
                fontWeight: FontWeight.w700
              ),
            ),
            const SizedBox(height: 28),
            CustomTextField(
              label: "Почта",
              hint: "***********@mail.com",
              onChange: onChange,
              controller: email
            ),
            const SizedBox(height: 56),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                      onPressed: (enableButton) ? () async {
                        showLoading(context);
                        presenter.pressSendCode(
                          email.text,
                          (_) {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (_) => OTPPage(email: email.text)),
                                (_) => false
                            );
                          },
                          (p0) => showErrorDialog(context, p0)
                        );
                      } : null,
                      child: Text(
                        "Отправить код",
                        style: Theme.of(context).textTheme.labelLarge
                      ),
                    )
                  ),
                  const SizedBox(height: 20),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (_) => const SignInPage()),
                              (_) => false
                      );
                    },
                    child: SizedBox(
                      width: double.infinity,
                      child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: [
                        TextSpan(
                            text: "Я вспомнил свой пароль! ",
                            style: TextStyle(
                              fontSize: 14,
                              color: colors.subText
                            )
                        ),
                        TextSpan(
                          text: "Вернуться",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: colors.accent
                          )
                        ),
                      ])),
                    ),
                  ),
                  const SizedBox(height: 32)
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
