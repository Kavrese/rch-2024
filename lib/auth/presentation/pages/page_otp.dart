import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:rch_2024/auth/domain/otp_presenter.dart';
import 'package:rch_2024/auth/presentation/pages/page_set_new_password.dart';
import 'package:rch_2024/common/app.dart';
import 'package:rch_2024/common/utils.dart';

class OTPPage extends StatefulWidget{

  final String email;

  const OTPPage({super.key, required this.email});

  @override
  State<OTPPage> createState() => _OTPPageState();
}

class _OTPPageState extends State<OTPPage> {

  OtpPresenter presenter = OtpPresenter();

  var enableButton = false;
  var isError = false;

  TextEditingController code = TextEditingController();

  @override
  void initState() {
    super.initState();
    presenter.startDelayDecrement((p0) {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 83),
            Text(
              "Верификация",
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w700,
                color: colors.text
              ),
            ),
            const SizedBox(height: 8),
            Text(
              "Введите 6-ти значный код из письма ",
              style: TextStyle(
                fontWeight: FontWeight.w500,
                color: colors.subText,
                fontSize: 14
              )
            ),
            const SizedBox(height: 52),
            Pinput(
              length: 6,
              controller: code,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              defaultPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFFA7A7A7)),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              focusedPinTheme: PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFF0560FA)),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              submittedPinTheme: (!isError) ? PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFF0560FA)),
                  borderRadius: BorderRadius.circular(0),
                ),
              ) : PinTheme(
                width: 32,
                height: 32,
                decoration: BoxDecoration(
                  border: Border.all(color: const Color(0xFFED3A3A)),
                  borderRadius: BorderRadius.circular(0),
                ),
              ),
              onChanged: (text){
                setState(() {
                  isError = false;
                  enableButton = text.length == 6;
                });
              },
            ),
            const SizedBox(height: 30),
            (presenter.lostSecond != 0) ? SizedBox(
              width: double.infinity,
              child: Text(
                "Получить код повторно через ${presenter.lostSecond}с",
                style: TextStyle(
                  fontSize: 14,
                  color: colors.subText
                ),
                textAlign: TextAlign.center,
              ),
            ) : GestureDetector(onTap: () async {
              showLoading(context);
              await presenter.resendCode(
                widget.email,
                (){
                  setState(() {
                    Navigator.pop(context);
                  });
                },
                (error){
                  Navigator.pop(context);
                  showErrorDialog(context, error);
                }
              );
            }, child: Align(
              alignment: Alignment.center,
              child: Text(
                "Получить новый код",
                style: TextStyle(
                  color: colors.accent,
                  fontWeight: FontWeight.w500
                ),
              ),
            )),
            const SizedBox(height: 84),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                      onPressed: (enableButton) ? () async {
                        await presenter.verifyCode(
                          widget.email,
                          code.text,
                          (_){
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (_) => const SetNewPasswordPage()),
                                (_) => false
                            );
                          },
                          (error){
                            setState(() {
                              isError = true;
                            });
                            showErrorDialog(context, error);
                          }
                        );
                      } : null,
                      child: Text("Сбросить пароль", style: Theme.of(context).textTheme.labelLarge)
                    ),
                  ),
                  const SizedBox(height: 14),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: SizedBox(
                      width: double.infinity,
                      child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(children: [
                            TextSpan(
                              text: "Я вспомнил свой пароль! ",
                              style: TextStyle(
                                fontSize: 14,
                                color: colors.subText
                              )
                            ),
                            TextSpan(
                              text: "Вернуться",
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: colors.accent
                              )
                            ),
                          ])),
                    ),
                  ),
                  const SizedBox(height: 32),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}