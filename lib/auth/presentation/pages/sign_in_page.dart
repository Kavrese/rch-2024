import 'package:flutter/material.dart';
import 'package:rch_2024/auth/domain/sign_in_presenter.dart';
import 'package:rch_2024/auth/presentation/pages/forgot_password_page.dart';
import 'package:rch_2024/auth/presentation/pages/holder.dart';
import 'package:rch_2024/auth/presentation/pages/sign_up_page.dart';
import 'package:rch_2024/common/app.dart';
import 'package:rch_2024/common/controllers/password_text_controller.dart';
import 'package:rch_2024/common/utils.dart';
import 'package:rch_2024/common/widgets/custom_text_field.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {

  late SignInPresenter presenter;

  @override
  void initState() {
    super.initState();
    SignInPresenter.init().then((value) async {
      presenter = value;
      await presenter.logoutIfAuth();
      presenter.checkExistAuthData((model) {
        presenter.useSavedAuthData(
          model,
          (p0) {
            Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (_) => const Holder()),
              (_) => false
            );
          },
          (p0) => showErrorDialog(context, p0)
        );
      });
    });
  }

  TextEditingController email = TextEditingController();
  PasswordTextController password = PasswordTextController();
  bool rememberPassword = false;

  bool isValid = false;

  void onChange(_){
    setState(() {
      isValid = presenter.isValid(
        email.text,
        password.text
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    var textTheme = Theme.of(context).textTheme;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: colors.background,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            const SizedBox(height: 83),
            Text(
              "Добро пожаловать",
              style: Theme.of(context).textTheme.titleLarge?.copyWith(
                color: colors.text
              ),
            ),
            const SizedBox(height: 8),
            Text(
              "Заполните почту и пароль чтобы продолжить",
              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                color: colors.subText
              ),
            ),
            const SizedBox(height: 24),
            CustomTextField(
              label: "Почта",
              hint: "***********@mail.com",
              controller: email,
              onChange: onChange
            ),
            const SizedBox(height: 24),
            CustomTextField(
              label: "Пароль",
              hint: "***********",
              enableObscure: true,
              controller: password,
              onChange: onChange
            ),
            const SizedBox(height: 18),
            Row(
              children: [
                SizedBox.square(
                  dimension: 22,
                  child: Transform.scale(
                    scale: 1.2,
                    child: Checkbox(
                      value: rememberPassword,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(6)
                      ),
                      onChanged: (newValue) {
                        setState(() {
                          rememberPassword = newValue!;
                        });
                      },
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Text(
                    "Запомнить меня",
                    style: textTheme.titleMedium?.copyWith(
                      color: colors.subText
                    )
                  )
                ),
                GestureDetector(
                  onTap: (){
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (_) => const ForgotPasswordPage())
                    );
                  },
                  child: Text(
                    "Забыли пароль?",
                    style: textTheme.titleMedium?.copyWith(
                      color: colors.accent
                    )
                  ),
                )
              ],
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                      onPressed: (isValid) ? (){
                        presenter.pressSignIn(
                          email.text,
                          password.getHashText(),
                          rememberPassword,
                          (_) {
                            Navigator.of(context).pushReplacement(
                              MaterialPageRoute(builder: (_) => const Holder())
                            );
                          },
                          (p0) => showErrorDialog(context, p0)
                        );
                      } : null,
                      child: Text("Войти", style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: colors.textAccent
                      ))
                    ),
                  ),
                  const SizedBox(height: 14),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (_) => const SignUpPage())
                      );
                    },
                    child: RichText(text: TextSpan(
                      children: [
                        TextSpan(
                          text: "У меня нет аккаунта! ",
                          style: TextStyle(fontSize: 14, color: colors.subText)
                        ),
                        TextSpan(
                          text: "Cоздать",
                          style: TextStyle(
                            fontSize: 14,
                            color: colors.accent,
                            fontWeight: FontWeight.w700
                          )
                        ),
                      ]
                    )),
                  ),
                  const SizedBox(height: 34),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}