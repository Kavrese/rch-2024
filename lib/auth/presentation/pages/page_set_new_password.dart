import 'package:flutter/material.dart';
import 'package:rch_2024/auth/domain/set_new_password_presenter.dart';
import 'package:rch_2024/auth/presentation/pages/sign_in_page.dart';
import 'package:rch_2024/common/app.dart';
import 'package:rch_2024/common/controllers/password_text_controller.dart';
import 'package:rch_2024/common/utils.dart';
import 'package:rch_2024/common/widgets/custom_text_field.dart';

class SetNewPasswordPage extends StatefulWidget {
  const SetNewPasswordPage({super.key});

  @override
  State<SetNewPasswordPage> createState() => _SetNewPasswordPageState();
}

class _SetNewPasswordPageState extends State<SetNewPasswordPage> {

  PasswordTextController password = PasswordTextController();
  PasswordTextController confirmPassword = PasswordTextController();

  SetNewPasswordPresenter presenter = SetNewPasswordPresenter();
  bool enableButton = false;

  void onChange(_){
    setState(() {
      enableButton = presenter.isValid(
        password.getHashText(),
        confirmPassword.getHashText()
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 83),
            Text(
              "Новый пароль",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  color: colors.text
              ),
            ),
            const SizedBox(height: 8),
            Text(
                "Введите новый пароль",
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: colors.subText,
                    fontSize: 14
                )
            ),
            const SizedBox(height: 28),
            CustomTextField(
                label: "Пароль",
                hint: "***********",
                enableObscure: true,
                controller: password,
                onChange: onChange
            ),
            const SizedBox(height: 24),
            CustomTextField(
                label: "Пароль",
                hint: "***********",
                enableObscure: true,
                controller: confirmPassword,
                onChange: onChange
            ),
            const SizedBox(height: 52),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: FilledButton(
                      onPressed: (enableButton) ? () async {
                        showLoading(context);
                        await presenter.pressConfirmNewPassword(
                          password.getHashText(),
                          (_){
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (_) => const SignInPage()),
                                (_) => false
                            );
                          },
                          (error){
                            Navigator.pop(context);
                            showErrorDialog(context, error);
                          }
                        );
                      } : null,
                      child: const Text("Подтвердить"),
                    ),
                  ),
                  const SizedBox(height: 14),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(builder: (_) => const SignInPage()),
                        (_) => false
                      );
                    },
                    child: SizedBox(
                      width: double.infinity,
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(children: [
                          TextSpan(
                            text: "Я вспомнил свой пароль! ",
                            style: TextStyle(
                                fontSize: 14,
                                color: colors.subText
                            )
                          ),
                          TextSpan(
                            text: "Вернуться",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: colors.accent
                            )
                          ),
                        ])),
                    ),
                  ),
                  const SizedBox(height: 32)
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
