import 'package:flutter/material.dart';
import 'package:rch_2024/common/colors.dart';

final colorsLights = LightColorsApp();
final light = ThemeData(
  inputDecorationTheme: InputDecorationTheme(
    helperStyle: TextStyle(
      color: colorsLights.disableAccent,
    ),
    hintStyle: TextStyle(
        color: colorsLights.hint
    ),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.subText)
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.subText)
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.error)
    ),
  ),
  textTheme: TextTheme(
    titleMedium: const TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w500,
    ),
    titleLarge: const TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.w500
    ),
    labelLarge: TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w700,
      color: colorsLights.textAccent
    )
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        textStyle: const TextStyle(fontWeight: FontWeight.bold),
        padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 15),
        side: BorderSide(color: colorsLights.accent),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4)
        ),
      )
  ),
  filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          textStyle: const TextStyle(fontWeight: FontWeight.bold),
          padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 15),
          backgroundColor: colorsLights.accent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
          disabledBackgroundColor: colorsLights.disableAccent,
          disabledForegroundColor: colorsLights.disableTextAccent
      )
  ),
  checkboxTheme: CheckboxThemeData(
    fillColor: MaterialStateProperty.resolveWith((states) {
      return (states.contains(MaterialState.selected)) ?
        colorsLights.accent : colorsLights.background;
    }),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(20)
    ),
    side: BorderSide(
      color: colorsLights.subText,
    )
  ),
  colorScheme: ColorScheme.fromSeed(seedColor: colorsLights.accent),
  useMaterial3: true,
);

final colorsDark = DarkColorsApp();
final dark = ThemeData(
  inputDecorationTheme: InputDecorationTheme(
    helperStyle: TextStyle(
      color: colorsLights.disableAccent,
    ),
    hintStyle: TextStyle(
        color: colorsLights.hint
    ),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.subText)
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.subText)
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: colorsLights.error)
    ),
  ),
  textTheme: TextTheme(
    titleMedium: TextStyle(
      color: colorsLights.subText,
      fontSize: 14,
      fontWeight: FontWeight.w500,
    ),
    labelMedium: TextStyle(
      color: colorsLights.hint,
      fontSize: 14,
      fontWeight: FontWeight.w500,
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
      style: OutlinedButton.styleFrom(
        textStyle: const TextStyle(fontWeight: FontWeight.bold),
        padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 15),
        side: BorderSide(color: colorsLights.accent),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(4)
        ),
      )
  ),
  filledButtonTheme: FilledButtonThemeData(
      style: FilledButton.styleFrom(
          textStyle: const TextStyle(fontWeight: FontWeight.bold),
          padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 15),
          backgroundColor: colorsLights.accent,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4)
          ),
          disabledBackgroundColor: colorsLights.disableAccent,
          disabledForegroundColor: colorsLights.disableTextAccent
      )
  ),
  colorScheme: ColorScheme.fromSeed(seedColor: colorsLights.accent),
  useMaterial3: true,
);