import 'package:flutter/material.dart';
import 'package:rch_2024/common/app.dart';
import 'package:rch_2024/common/theme.dart';

class CustomTextField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final Function(String)? onChange;

  const CustomTextField(
    {
      super.key,
      required this.label,
      required this.hint,
      required this.controller,
      this.enableObscure = false,
      this.onChange
    }
  );

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  bool isObscure = true;

  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColorsApp(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(
            color: colors.subText
          ),
        ),
        const SizedBox(height: 8),
        TextField(
          obscureText: (widget.enableObscure) ? isObscure : false,
          style: Theme.of(context).textTheme.titleMedium?.copyWith(
            color: colors.text
          ),
          obscuringCharacter: "*",
          controller: widget.controller,
          onChanged: widget.onChange,
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
            hintText: widget.hint,
            hintStyle: Theme.of(context).textTheme.titleMedium?.copyWith(
              color: colors.hint
            ),
            suffixIcon: (widget.enableObscure)
              ? GestureDetector(
                onTap: (){
                  setState(() {
                    isObscure = !isObscure;
                  });
                },
                child: Image.asset("assets/eye-slash.png"),
              )
              : null,
          ),
        ),
      ],
    );
  }
}
