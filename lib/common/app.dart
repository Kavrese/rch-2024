import 'package:flutter/material.dart';
import 'package:rch_2024/auth/presentation/pages/sign_in_page.dart';
import 'package:rch_2024/common/colors.dart';
import 'package:rch_2024/common/theme.dart';

class MyApp extends StatefulWidget {
  MyApp({super.key});

  var isLightTheme = true;

  void changeDifferentTheme(BuildContext context) {
    isLightTheme = !isLightTheme;
    context.findAncestorStateOfType<_MyAppState>()!.onChangeTheme();
  }

  static MyApp of(BuildContext context){
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }

  ColorsApp getColorsApp(BuildContext context){
    return (isLightTheme) ? colorsLights : colorsDark;
  }

  ThemeData getCurrentTheme(){
    return (isLightTheme) ? light : dark;
  }

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void onChangeTheme() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: widget.getCurrentTheme(),
      home: const SignInPage(),
    );
  }
}